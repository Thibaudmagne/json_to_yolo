import json
import os.path
import shutil
#Chemin à modifier / doit mener à la racine ou l'on veut récupérer les informations (json / photos)
PATH = "dossier_test/"

# Fonction permettant de récupérer les différents fichiers d'un dossier
def recuperationFichier(path): 
    fichier=[] 
    for root,dirs, files in os.walk(path): 
        for i in files: 
            fichier.append(os.path.join(root, i))
    return fichier


#Cette fonction à pour but de créer un export de fichier .txt contenant un dictionnaire avec en indice les différentes
#classes et en valeurs leurs idclass
def export_ListClass(dossier_json,nom_fichier_export = "tableau.json"):
    list_class = []
    for photo in dossier_json :
        with open(photo) as json_file:
            data = json.load(json_file)
            for objects in data['objects'] :
                if objects['classTitle'] not in list_class:
                    list_class.append(objects['classTitle'])

    listeClassFile = {x: i for i, x in enumerate(list_class)}
    #Je crée un dossier pour chaque type de classes afin d'y insérer ler images dans json_to_yolo()
    for classes in list_class : os.makedirs("photos/" + classes, exist_ok=True)
    with open("annotation/" + nom_fichier_export, 'w') as outfile:
        json.dump(listeClassFile, outfile)

#Fonction permettant de récupérer les valeurs de mon fichier contenant la liste de classes.
def import_ListClass(nom_fichier_listClass = "annotation/tableau.json"):
    with open(nom_fichier_listClass) as json_file:
        data = json.load(json_file)
    return data

def creation_structure_dossier():
    os.makedirs("annotation", exist_ok=True)
    os.makedirs("photos", exist_ok=True)
    os.makedirs("labels", exist_ok=True)
    
    

#Fonction permettant de transformer mes valeurs du fichier json au bon format yolo(Darknet)
def convert(size,boxe):
    w = boxe[1]-boxe[0]
    h = boxe[3]-boxe[2]
    dw = 1.0/size[0]
    dh = 1.0/size[1]
    x = ((boxe[0]+w/2))*dw
    y = ((boxe[2]+h/2))*dh
    w = w*dw
    h =h*dh
    result =[x,y,w,h]
    return result


#Fonction permettant de récupérer [x_min,y_min,width_box,height_box,x_max,y_max] d'un format darknet
def deconvert(size,box):
    width = size[0]
    height = size[1]
    xmin = (box[0]-(box[2]/2))*width
    ymin = (box[1]-box[3]/2)*height
    width_box = box[2]*width
    height_box = box[3]*height
    x_max = xmin + width_box
    y_max = ymin + height_box
    return_value = [xmin, ymin, width_box, height_box,x_max,y_max]
    return return_value 



def json_To_Yolo(dossier_json):
    liste_nom_photo=[]
    listClass = import_ListClass("annotation/tableau.json") 
    liste_photo = recuperationFichier(PATH + "img")
    for i in liste_photo :
        racine_photo = i.split("\\")[0]
        liste_nom_photo.append(i.split("\\")[-1])
    for info in dossier_json :
        with open(info) as json_file:
            
            verification_fichier_json = info.split("\\")[-1].replace(".json","")
            #permet de récupérer le nom du fichier json(.....png.json) et de le transformer en format txt
            nom_fichier_json = info.split("\\")[-1].replace("json","txt").replace('.png',"")
            
            #Je vérifie si le fichier json à bien une image du même nom, et si oui je peux récuprer ces informations
            if verification_fichier_json in liste_nom_photo :
                url_image = racine_photo + "\\\\" + verification_fichier_json

                file = open("labels/"+nom_fichier_json,"w")
                data = json.load(json_file)
                for objects in data['objects'] :
                    id_class = listClass[objects['classTitle']]
                    shutil.copy(url_image, "photos/"+objects["classTitle"])
                    x_min = objects["points"]["exterior"][0][0]
                    y_min = objects["points"]["exterior"][0][1]
                    x_max = objects["points"]["exterior"][1][0]
                    y_max = objects["points"]["exterior"][1][1]
                    width = data['size']['width']
                    height = data['size']['height']
                    size = [width,height]
                    boxe = [x_min,x_max,y_min,y_max]
                    yolo_format = convert(size,boxe)
                    file.write(str(id_class) + " " + str(yolo_format[0])+ " " + str(yolo_format[1])+ " " + str(yolo_format[2])+ " " + str(yolo_format[3]) +"\n")
                file.close()
            else :
                print("le fichier " + verification_fichier_json + ".json ne correspond à aucune image") 